let paragraphs = document.getElementsByTagName('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = '#ff0000';
}

let optionsList = document.getElementById('optionsList');
console.log(optionsList);
let parent = optionsList.parentElement;
console.log(parent);
let child = optionsList.childNodes;
console.log(child);
child.forEach((node) => console.log(node.nodeType));


let testParagraph = document.getElementById('testParagraph');
// console.log(testParagraph);
testParagraph.innerText = 'This is a paragraph';

let mainHeader = document.querySelectorAll('.main-header li');
console.log(mainHeader);
Array.from(mainHeader).forEach((item) => {
    item.className = 'nav-item';
});

let sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach((item) => item.classList.remove('section-title'));
